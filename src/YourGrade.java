import java.util.Scanner;

public class YourGrade {
    public static void main(String[] args) {
        String strScore;
        int score;
        Scanner sc = new Scanner(System.in); // รับinput
        System.out.print("Please input your score : ");
        strScore = sc.next();
        score = Integer.parseInt(strScore);
    
        if (score >= 80){
            System.out.println("Grade: A");
        } else if(score >= 75) {
            System.out.println("Grade: B+");
        } else if(score >= 70) {
            System.out.println("Grade: B");
        } else if(score >= 65) {
            System.out.println("Grade: C+");
        } else if(score >= 60) {
            System.out.println("Grade: C");
        } else if(score >= 55) {
            System.out.println("Grade: D+");
        } else if(score >= 50) {
            System.out.println("Grade: D");
        } else if(score < 50) {
            System.out.println("Grade: F");
        }
    }
}
